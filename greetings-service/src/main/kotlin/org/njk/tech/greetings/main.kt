package org.njk.tech.greetings

import arrow.core.None
import arrow.core.right
import arrow.core.some
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.application.log
import io.ktor.features.*
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.JacksonConverter
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.server.engine.EngineAPI
import org.njk.tech.greetercommons.JsonMapper
import org.njk.tech.greetercommons.handleRequest
import org.njk.tech.greetercommons.initialise
import java.util.*
import java.util.concurrent.TimeUnit

data class Greeting(
  val msg: String
)
val greetings = listOf(
  Greeting("Hello"),
  Greeting("Bonjour"),
  Greeting("Hej"),
  Greeting("Good Morning"),
  Greeting("Goed Morgen")
)

fun mainModule(): Application.() -> Unit = {
  install(AutoHeadResponse)
  install(CallId) {
    generate { UUID.randomUUID().toString() }
    verify { callId -> callId.isNotEmpty() }
    replyToHeader("X-Call-ID")
  }
  install(Compression) {
    gzip {
      priority = 1.0
    }
    deflate {
      priority = 10.0
      minimumSize(1024) // condition
    }
  }
  install(ConditionalHeaders)
  install(ContentNegotiation) {
    register(ContentType.Application.Json, JacksonConverter(JsonMapper.defaultMapper))
  }
  install(CORS) {
    method(HttpMethod.Options)
    method(HttpMethod.Put)
    method(HttpMethod.Delete)
    method(HttpMethod.Patch)
    header(HttpHeaders.Authorization)
    allowCredentials = true
    anyHost() // @TODO: Don't do this in production if possible. Try to limit it.
  }
  install(StatusPages) {
    exception<Throwable> { cause ->
      call.respondText(
        "Something went wrong, Please try again or contact Vayana support!",
        ContentType.Text.Plain,
        HttpStatusCode.InternalServerError
      )
      log.error("Unknown error", cause)
    }
  }
  routing {
    route("/greetings-service") {
      randomName()
    }
  }
}

fun Route.randomName() {
  get("/greeting") {
    handleRequest() {
      val randomIdx = greetings.indices.shuffled().first()
      greetings[randomIdx].right()
    }
  }
}

object Main {
  @EngineAPI
  @JvmStatic
  fun main(args: Array<String>) {
    initialise(
      host = "0.0.0.0",
      port = 10091,
      gracePeriod = 5,
      timeout = 5,
      timeUnit = TimeUnit.SECONDS,
      module = mainModule(),
      stopAtShutdown = true.some(),
      stopTimeout = 60000L.some(),
      accessLogbackFile = None
    )
  }
}
