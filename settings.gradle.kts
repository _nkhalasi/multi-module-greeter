rootProject.name = "multi-module-greeter"
include("greeter-service")
include("greetings-service")
include("names-service")
include("greeter-commons")

