import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import com.bmuschko.gradle.docker.tasks.image.DockerBuildImage
import com.bmuschko.gradle.docker.tasks.image.DockerPushImage

plugins {
  kotlin("jvm") version "1.3.61"
  kotlin("kapt") version "1.3.61"
  java
  idea
  id("com.github.johnrengelman.shadow") version "5.2.0"
  id("com.bmuschko.docker-remote-api") version "6.0.0"
  id("com.patdouble.awsecr") version "0.6.0"
}

group = "org.njk.tech"

allprojects {
  val logback_version: String by project
  val jackson_version: String by project
  val ktor_version: String by project
  val arrow_version: String by project
  val junit5_version: String by project

  repositories {
    mavenCentral()
    jcenter()
  }

  apply(plugin = "org.jetbrains.kotlin.jvm")
  apply(plugin = "org.jetbrains.kotlin.kapt")

  dependencies {
    runtimeOnly(kotlin("reflect"))
    implementation(kotlin("stdlib"))

    implementation("ch.qos.logback", "logback-classic", logback_version)
    implementation("ch.qos.logback", "logback-access", logback_version)
    implementation("io.ktor", "ktor-server-jetty", ktor_version)
    implementation("io.ktor", "ktor-server-core", ktor_version)
    implementation("io.ktor", "ktor-jackson", ktor_version)

    implementation("com.fasterxml.jackson.module", "jackson-module-kotlin", jackson_version)
    implementation("com.fasterxml.jackson.module", "jackson-modules-java8", jackson_version)
    implementation("com.fasterxml.jackson.datatype", "jackson-datatype-jsr310", jackson_version)

    implementation("io.arrow-kt", "arrow-core", arrow_version)
    implementation("io.arrow-kt", "arrow-syntax", arrow_version)
    implementation("io.arrow-kt", "arrow-fx", arrow_version)
    implementation("io.arrow-kt", "arrow-mtl", arrow_version)
    kapt("io.arrow-kt", "arrow-meta", arrow_version)

    testImplementation("io.ktor", "ktor-server-tests", ktor_version)
    testImplementation("org.junit.jupiter", "junit-jupiter-api", junit5_version)
    testRuntimeOnly("org.junit.jupiter", "junit-jupiter-engine", junit5_version)

  }

  tasks {
    withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
      kotlinOptions.jvmTarget = "1.8"
    }
  }
}


subprojects {
  version = "1.0"

  val buildEnv: String? by project
  val buildConfig: org.njk.tech.greeterbuild.EnvironmentConfig by project.extra {
    org.njk.tech.greeterbuild.buildConfig(buildEnv ?: "dev")!!
  }

  tasks {
    withType<ShadowJar> {
      manifest {
        attributes["Main-Class"] = "org.njk.tech.greeter.Main"
      }

      isZip64 = true

      /**
       * Merge service files to prevent the 'java.lang.NoClassDefFoundError' because
       * the runtime could not initialize class 'org.eclipse.jetty.http.HttpParser'
       *
       * This is likely because ktor and jaq both depend on jetty, making it
       * ambiguous as to which class to use to configure library or application behaviour.
       *
       * Ref: https://imperceptiblethoughts.com/shadow/configuration/merging/#merging-service-descriptor-files
       */
      mergeServiceFiles()
    }

    register<Copy>("copyShadowJar") {
      dependsOn("shadowJar")
      from("$buildDir/libs/${project.name}-${project.version}-all.jar")
      into("$buildDir/docker")
      rename("(.+)-all(.+)", "$1$2")
    }

    register<DockerBuildImage>("buildDockerImage") {
      dependsOn("createDockerfile")
      images.add("njk-${project.name}:${project.version}")
      images.add("njk-${project.name}:latest")
      images.add("${buildConfig.ecrFqdn}/${project.name}:latest")
    }

    register<DockerPushImage>("pushDockerImage") {
      //  ./gradlew -Daws.accessKeyId=MY_ACCESS_ID -Daws.secretKey=MY_SECRET
      dependsOn("buildDockerImage")
      registryCredentials {
        url.set("https://${buildConfig.ecrFqdn}")
      }
      images.add("${buildConfig.ecrFqdn}/${project.name}:latest")
    }

    test {
      useJUnitPlatform()
    }
  }
}
