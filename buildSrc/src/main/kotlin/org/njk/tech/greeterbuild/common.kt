package org.njk.tech.greeterbuild

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.databind.SerializationFeature
import java.security.InvalidParameterException


val mapper: ObjectMapper = ObjectMapper().registerModule(KotlinModule()).apply {
  enable(SerializationFeature.INDENT_OUTPUT)
}

enum class Environment(val code: String) {
  Development("dev"),
  Testing("test"),
  Production("prod");

  companion object {
    private val map: Map<String, Environment> = values().map { it.code to it }.toMap()
    operator fun invoke(code: String) = map[code] ?: throw InvalidParameterException("Err! Invalid environment")
  }
}

data class EnvironmentConfig(
  val ecrFqdn: String
)

val buildConfigs = mapOf(
  "dev" to EnvironmentConfig("078559448331.dkr.ecr.ap-south-1.amazonaws.com"),
  "test" to EnvironmentConfig("078559448331.dkr.ecr.ap-south-1.amazonaws.com"),
  "prod" to EnvironmentConfig("dummy-for-now")
)
