package org.njk.tech.greeterbuild

fun buildConfig(buildEnv: String): EnvironmentConfig? =
  buildConfigs[buildEnv]
