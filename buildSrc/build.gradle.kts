plugins {
  `kotlin-dsl`
  java
}

group = "org.njk.tech"
version = "1.0"

repositories {
  mavenCentral()
  jcenter()
}

val jackson_version = "2.10.1"

dependencies {
  implementation("org.jetbrains.kotlin", "kotlin-stdlib-jdk8", "1.3.61")
  implementation("com.fasterxml.jackson.module", "jackson-module-kotlin", jackson_version)
  implementation("com.fasterxml.jackson.module", "jackson-modules-java8", jackson_version)
  implementation("com.fasterxml.jackson.datatype", "jackson-datatype-jsr310", jackson_version)
}
