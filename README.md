# Collection of Greeter services

## Greetings Service

Responsible for providing different greeting message each time it is invoked.

```
curl -v http://localhost:10091/greetings-service/greeting
```

## Names Service

Responsible for providing different name each time it is invoked.

```
curl -v http://localhost:10092/names-service/name
```

## Greeter Service

Responsible for invoking `Greetings` service and `Names` service and provide a full greeting message in response.

```
curl -v http://localhost:10090/greeter-service/greeting
```

### Build fatjar and create docker image

```
gradle clean :greeter-service:buildDockerImage
```

### Build and push docker image to AWS ECR

```
gradle -Daws.accessKeyId=MY_ACCESS_ID -Daws.secretKey=MY_SECRET_KEY clean :greeter-service:pushDockerImage
```

- The ECR URLs are configured in `buildSrc/src/main/kotlin/org/njk/tech/greeterbuild/common.kt`
- Ignore the exception related to missing `$HOME/.docker/config.json`
