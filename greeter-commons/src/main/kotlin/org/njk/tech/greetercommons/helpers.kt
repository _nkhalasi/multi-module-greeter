package org.njk.tech.greetercommons

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import arrow.core.some

suspend fun <A, E : Enum<E>> catche(e: E, message: String, args: Map<String, Any?> = emptyMap(), f: suspend () -> Either<Fault<E>, A>): Either<Fault<E>, A> =
  try {
    f()
  } catch (t: Throwable) {
    t.printStackTrace()
    Fault(e, message, cause = t.some()).left()
  }

suspend fun <A, E : Enum<E>> catcht(e: E, message: String, args: Map<String, Any?> = emptyMap(), f: suspend () -> A): Either<Fault<E>, A> =
  try {
    f().right()
  } catch (t: Throwable) {
    t.printStackTrace()
    Fault(e, message, cause = t.some()).left()
  }
