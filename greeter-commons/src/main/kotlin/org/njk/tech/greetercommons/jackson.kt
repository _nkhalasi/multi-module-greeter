package org.njk.tech.greetercommons

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import java.text.SimpleDateFormat

object JsonMapper {
  val defaultMapper: ObjectMapper = jacksonObjectMapper()

  init {
    val yyyyDashMMDashddFormat = SimpleDateFormat("yyyy-MM-dd")
    defaultMapper.apply {
      configure(SerializationFeature.INDENT_OUTPUT, true)
      registerModule(JavaTimeModule())
      dateFormat = yyyyDashMMDashddFormat
      setSerializationInclusion(JsonInclude.Include.NON_NULL)
    }
  }
}
