package org.njk.tech.greetercommons

import arrow.core.Either
import com.fasterxml.jackson.databind.ObjectMapper
import io.ktor.application.ApplicationCall
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.response.header
import io.ktor.response.respondFile
import io.ktor.response.respondText
import org.slf4j.LoggerFactory
import java.io.File

private val log = LoggerFactory.getLogger("org.njk.tech.greetercommons.response")

private fun <E : Enum<E>> Fault<E>.toJson(mapper: ObjectMapper): String =
  mapper.writeValueAsString(mapOf("error" to mapOf("message" to message, "args" to args)))
    .apply { println(this); log.error(this@toJson.message, this@toJson.cause) }


@Suppress("NOTHING_TO_INLINE")
suspend inline fun <E : Enum<E>, T> Either<Fault<E>, T>.respondJson(
  mapper: ObjectMapper,
  call: ApplicationCall,
  onErrorHttpStatusCode: (Fault<E>) -> HttpStatusCode
): Either<Fault<E>, T> =
  apply {
    fold({ fault ->
      call.respondFault(mapper, fault, onErrorHttpStatusCode(fault))
    }, { data ->
      when (data) {
        is File -> {
          if (data.exists()) {
            call.response.header("Content-Disposition", "attachment; filename=\"${data.name}\"")
            call.response.status(HttpStatusCode.OK)
            call.respondFile(data)
          } else {
            call.response.status(HttpStatusCode.NotFound)
          }
        }
        else    -> {
          mapper.writeValueAsString(mapOf("data" to data)).let { result ->
            call.respondText(result, ContentType.Application.Json, HttpStatusCode.OK)
          }
        }
      }
    })
  }

suspend fun <E : Enum<E>> ApplicationCall.respondFault(mapper: ObjectMapper, fault: Fault<E>, statusCode: HttpStatusCode) =
  respondText(fault.toJson(mapper), ContentType.Application.Json, statusCode)
