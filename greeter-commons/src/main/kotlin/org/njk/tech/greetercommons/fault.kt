package org.njk.tech.greetercommons

import arrow.core.None
import arrow.core.Option
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger

open class Fault<E : Enum<E>>(
  val type: E,
  val message: String,
  val args: Map<String, Any?> = EMPTY_MAP,
  val cause: Option<Throwable> = None,
  @Suppress("UNCHECKED_CAST")
  val contents: List<Fault<E>> = EMPTY_LIST as List<Fault<E>>
) {
  companion object {
    val EMPTY_MAP = mapOf<String, Any?>()
    val EMPTY_LIST = emptyList<Fault<*>>()
  }

  override fun toString() = "Fault(${type}:${message}:${args}:${cause})"
  fun toLogString() = "${type}:${message}:${args}"
}


// this helps logging fault as error
fun <E : Enum<E>> Logger.fault(fault: Fault<E>, mapper: ObjectMapper) =
  mapper.writeValueAsString(mapOf("message" to fault.message, "args" to fault.args)).run {
    error(this, fault.cause.orNull())
  }

data class FaultReport<E : Enum<E>>(
  val faultType: E,
  val message: String,
  val args: Map<String, Any?>,
  val cause: Option<Throwable>
)

fun <E : Enum<E>> Fault<E>.toReport() = FaultReport(type, message, args, cause)
