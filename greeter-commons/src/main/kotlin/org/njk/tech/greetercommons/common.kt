package org.njk.tech.greetercommons

import arrow.core.Either
import arrow.core.extensions.either.applicativeError.handleError
import arrow.core.left
import arrow.core.right
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.features.callId
import io.ktor.http.HttpStatusCode
import io.ktor.util.pipeline.PipelineContext
import io.ktor.util.toMap
import org.slf4j.LoggerFactory
import java.time.LocalDateTime

val LOG = LoggerFactory.getLogger("org.njk.tech.greetercommons.common")

enum class FaultType {
  RuntimeError,
  ClientError,
  ConfigurationError
}

data class CallContext(
  val callId: String,
  val requestHeaders: MutableMap<String, Any> = mutableMapOf(),
  val requestTime: LocalDateTime = LocalDateTime.now(),
  val responseTime: LocalDateTime? = null
)

fun createCallContext(callId: String): Either<Fault<FaultType>, CallContext> =
  CallContext(callId).right()

private suspend fun <T> PipelineContext<*, ApplicationCall>.dispatch(
  callId: String?,
  block: suspend (CallContext) -> Either<Fault<FaultType>, T>
) {
  createCallContext(callId!!).fold(
    { it.left() },
    {
      it.requestHeaders.putAll(call.request.headers.toMap())
      executeBlock(it, block)
    }
  ).respondJson(JsonMapper.defaultMapper, call) {
    when(it.type) {
      FaultType.RuntimeError -> HttpStatusCode.BadRequest
      FaultType.ClientError -> HttpStatusCode.BadRequest
      FaultType.ConfigurationError -> HttpStatusCode.InternalServerError
      else -> HttpStatusCode.InternalServerError
    }
  }
}

private suspend fun <T> executeBlock(
  cc: CallContext,
  block: suspend (CallContext) -> Either<Fault<FaultType>, T>
): Either<Fault<FaultType>, T> =
  catche(FaultType.RuntimeError, "err-dispatching-request") {
    block(cc).apply {
      this.handleError {
        LOG.error(it.message, it.cause.orNull())
      }
    }
  }

suspend fun <T> PipelineContext<*, ApplicationCall>.handleRequest(
  block: suspend (CallContext) -> Either<Fault<FaultType>, T>
) {
  val callId = call.callId ?: ""
  dispatch(callId, block)
}
