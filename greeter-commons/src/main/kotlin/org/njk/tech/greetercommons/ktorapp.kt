package org.njk.tech.greetercommons

import arrow.core.*
import ch.qos.logback.access.jetty.RequestLogImpl
import io.ktor.application.Application
import io.ktor.server.engine.EngineAPI
import io.ktor.server.engine.embeddedServer
import io.ktor.server.jetty.Jetty
import io.ktor.server.jetty.JettyApplicationEngine
import org.eclipse.jetty.server.handler.RequestLogHandler
import org.eclipse.jetty.server.handler.StatisticsHandler
import java.security.Security
import java.util.concurrent.TimeUnit

@EngineAPI
fun initialise(
  host: String,
  port: Int,
  gracePeriod: Long,
  timeout: Long,
  timeUnit: TimeUnit,
  module: Application.() -> Unit,
  stopAtShutdown : Option<Boolean> = None,
  stopTimeout : Option<Long> = None,
  accessLogbackFile: Option<String> = None
): Either<Fault<FaultType>, JettyApplicationEngine> {
  Security.setProperty("crypto.policy", "unlimited")
  return embeddedServer(Jetty, host = host, port = port, configure = {
    configureServer = {
      stopTimeout.map { this.stopTimeout = it }
      stopAtShutdown.map { this.stopAtShutdown = it }
      val existingHandler = handler

      handler = StatisticsHandler().apply {
        requestLog = RequestLogImpl().apply {
          accessLogbackFile.map {
            setFileName(it)
            start()
          }
        }
        handler = RequestLogHandler().apply {
          handler = existingHandler
        }
      }
    }
  }, module = module).let {
    try {
      it.start(wait = true)
      it.right()
    } catch (e: Exception) {
      it.stop(gracePeriod, timeout, timeUnit)
      Fault(FaultType.RuntimeError, "err-could-not-start-server").left()
    }
  }
}
