package org.njk.tech.greeter

import arrow.core.*
import com.fasterxml.jackson.module.kotlin.readValue
import io.ktor.application.*
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.response.HttpResponse
import io.ktor.client.response.readText
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.jackson.JacksonConverter
import io.ktor.response.respondText
import io.ktor.routing.*
import io.ktor.server.engine.EngineAPI
import kotlinx.coroutines.*
import org.njk.tech.greetercommons.*
import java.util.*
import java.util.concurrent.TimeUnit


fun mainModule(): Application.() -> Unit = {
  install(AutoHeadResponse)
  install(CallId) {
    generate { UUID.randomUUID().toString() }
    verify { callId -> callId.isNotEmpty() }
    replyToHeader("X-Call-ID")
  }
  install(Compression) {
    gzip {
      priority = 1.0
    }
    deflate {
      priority = 10.0
      minimumSize(1024) // condition
    }
  }
  install(ConditionalHeaders)
  install(ContentNegotiation) {
    register(ContentType.Application.Json, JacksonConverter(JsonMapper.defaultMapper))
  }
  install(CORS) {
    method(HttpMethod.Options)
    method(HttpMethod.Put)
    method(HttpMethod.Delete)
    method(HttpMethod.Patch)
    header(HttpHeaders.Authorization)
    allowCredentials = true
    anyHost() // @TODO: Don't do this in production if possible. Try to limit it.
  }
  install(StatusPages) {
    exception<Throwable> { cause ->
      call.respondText(
        "Something went wrong, Please try again or contact Vayana support!",
        ContentType.Text.Plain,
        HttpStatusCode.InternalServerError
      )
      log.error("Unknown error", cause)
    }
  }
  routing {
    route("/greeter-service") {
      randomGreeting()
    }
  }
}

fun Route.randomGreeting() {
  get("/greeting") {
    handleRequest() {
      fetchRandomGreeting()
    }
  }
}

private suspend fun fetchRandomGreeting() = coroutineScope<Either<Fault<FaultType>, String>> {
  val client = HttpClient()
  val greetingRequest = async { client.get<HttpResponse>("http://localhost:10091/greetings-service/greeting") }
  val nameRequest = async { client.get<HttpResponse>("http://localhost:10092/names-service/name") }

  val greetingResponse = greetingRequest.await()
  val nameResponse = nameRequest.await()
  client.close()

  if (greetingResponse.status == HttpStatusCode.OK && nameResponse.status == HttpStatusCode.OK) {
    val gr = JsonMapper.defaultMapper.readValue<Map<String, Any>>(greetingResponse.readText())
    val nr = JsonMapper.defaultMapper.readValue<Map<String, Any>>(nameResponse.readText())
    (gr["data"].toString() + nr["data"].toString()).right()
  } else {
    Fault(FaultType.ClientError, "err-invoking-downstream-service").left()
  }
}

object Main {
  @EngineAPI
  @JvmStatic
  fun main(args: Array<String>) {
    initialise(
      host = "0.0.0.0",
      port = 10090,
      gracePeriod = 5,
      timeout = 5,
      timeUnit = TimeUnit.SECONDS,
      module = mainModule(),
      stopAtShutdown = true.some(),
      stopTimeout = 60000L.some(),
      accessLogbackFile = None
    )
  }
}
