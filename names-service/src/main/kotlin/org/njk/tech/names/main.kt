package org.njk.tech.names

import arrow.core.None
import arrow.core.right
import arrow.core.some
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.jackson.JacksonConverter
import io.ktor.response.respondText
import io.ktor.routing.*
import io.ktor.server.engine.EngineAPI
import org.njk.tech.greetercommons.JsonMapper.defaultMapper
import org.njk.tech.greetercommons.handleRequest
import org.njk.tech.greetercommons.initialise
import java.util.*
import java.util.concurrent.TimeUnit

data class Name(val firstName: String, val gender: String)
val names = listOf(
  Name("Katherina", "female"),  Name("Vergie", "female"),
  Name("Ron", "male"),  Name("Milan", "male"),
  Name("Louanne", "female"), Name("Renita", "female"),
  Name("Freeman", "male"), Name("Micah", "male"),
  Name("Olivia", "female"), Name("Kylie", "female"),
  Name("Will", "male"), Name("Billie", "male"),
  Name("Rosanna", "female"), Name("Sumiko", "female"),
  Name("Shawn", "male"), Name("Fernando", "male"),
  Name("Josefa", "female"), Name("Arminda", "female"),
  Name("Bertman", "male"), Name("Randell", "male"),
  Name("Delphia", "female"), Name("Pauline", "female"),
  Name("Raul", "male"), Name("Garfield", "male"),
  Name("Dina", "female"), Name("Analisa", "female"),
  Name("Johnny", "male"), Name("Hank", "male")
)

fun mainModule(): Application.() -> Unit = {
  install(AutoHeadResponse)
  install(CallId) {
    generate { UUID.randomUUID().toString() }
    verify { callId -> callId.isNotEmpty() }
    replyToHeader("X-Call-ID")
  }
  install(Compression) {
    gzip {
      priority = 1.0
    }
    deflate {
      priority = 10.0
      minimumSize(1024) // condition
    }
  }
  install(ConditionalHeaders)
  install(ContentNegotiation) {
    register(ContentType.Application.Json, JacksonConverter(defaultMapper))
  }
  install(CORS) {
    method(HttpMethod.Options)
    method(HttpMethod.Put)
    method(HttpMethod.Delete)
    method(HttpMethod.Patch)
    header(HttpHeaders.Authorization)
    allowCredentials = true
    anyHost() // @TODO: Don't do this in production if possible. Try to limit it.
  }
  install(StatusPages) {
    exception<Throwable> { cause ->
      call.respondText(
        "Something went wrong, Please try again or contact Vayana support!",
        ContentType.Text.Plain,
        HttpStatusCode.InternalServerError
      )
      log.error("Unknown error", cause)
    }
  }
  routing {
    route("/names-service") {
      randomName()
    }
  }
}

fun Route.randomName() {
  get("/name") {
    handleRequest() {
      val randomIdx = names.indices.shuffled().first()
      names[randomIdx].right()
    }
  }
}

object Main {
  @EngineAPI
  @JvmStatic
  fun main(args: Array<String>) {
    initialise(
      host = "0.0.0.0",
      port = 10092,
      gracePeriod = 5,
      timeout = 5,
      timeUnit = TimeUnit.SECONDS,
      module = mainModule(),
      stopAtShutdown = true.some(),
      stopTimeout = 60000L.some(),
      accessLogbackFile = None
    )
  }
}
