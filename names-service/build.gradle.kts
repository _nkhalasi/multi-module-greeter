import com.bmuschko.gradle.docker.tasks.image.Dockerfile

plugins {
  kotlin("jvm")
  kotlin("kapt")
  id("com.github.johnrengelman.shadow")
  id("com.bmuschko.docker-remote-api")
  id("com.patdouble.awsecr")
}

dependencies {
  implementation(project(":greeter-commons"))
}

tasks {
  register<Dockerfile>("createDockerfile") {
    dependsOn("copyShadowJar")

    val appName = project.name
    val deployUser = "deploy"
    val deployGroup = "deploy"
    val deployHome = "/home/$deployUser"
    val deployBase = "$deployHome/apps/$appName"
    val deployArtifact = "$appName-${project.version}.jar"

    from("adoptopenjdk/openjdk13:alpine-slim")
    label(mapOf("maintainer" to "Naresh Khalasi 'naresh.khalasi@gmail.com'"))
    environmentVariable(
      mapOf(
        "APP_DEPLOY_USER" to deployUser,
        "APP_DEPLOY_GROUP" to deployGroup,
        "APP_DEPLOY_HOME" to deployHome,
        "APP_DEPLOY_BASE_DIR" to deployBase,
        "APP_DEPLOY_ARTIFACT" to deployArtifact
      )
    )
    runCommand("""
      addgroup -S $deployGroup \
      && adduser -S $deployUser -G $deployGroup -h $deployHome \
      && mkdir -p $deployBase/bin $deployBase/logs $deployBase/datafiles $deployBase/config \
      && chown -R $deployUser:$deployGroup $deployBase/bin  $deployBase/logs  $deployBase/datafiles  $deployBase/config \
      && apk add --no-cache dumb-init \
      && rm -rf /var/cache/apk/*
    """.trimIndent())
    copyFile(provider {
      Dockerfile.CopyFile(deployArtifact, "$deployBase/bin/$deployArtifact")
        .withChown("$deployUser:$deployGroup") as Dockerfile.CopyFile
    })
    volume(deployBase)
    entryPoint("dumb-init")
    user(deployUser)
    defaultCommand("/bin/sh", "-c", "java -jar $deployBase/bin/$deployArtifact")
  }

}
